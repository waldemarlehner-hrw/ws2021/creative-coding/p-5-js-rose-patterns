
import { Pane } from "tweakpane";
import DataState from "./DataState";

class ConfigurationHelper {
	public static Setup(data: DataState) : Pane {
		const pane = new Pane(); 
        const animationPane = pane.addFolder({title: "Animation"})
        animationPane.addInput(data, "shouldAnimate", {label: "Should Animate"})
        animationPane.addInput(data, "currentAngle", {label: "Current Angle", min: 0})
        animationPane.addInput(data, "animationSpeed", {label: "Animation Speed (°/s)", min: -360, max: 360, step: 1})

        const roseParams = pane.addFolder({title: "Rose Params"})
        roseParams.addInput(data, "primaryColor", {alpha: false, view: "color", label: "Rose Color"})
        roseParams.addInput(data, "backgroundColor", {alpha: true, view: "color", label: "Background Color"})
        roseParams.addInput(data, "degreeMultiplier", {label: "Degree Multiplier", min: 0, max: 10})
        
        return pane;
	}

}

export default ConfigurationHelper;
export {ConfigurationHelper};