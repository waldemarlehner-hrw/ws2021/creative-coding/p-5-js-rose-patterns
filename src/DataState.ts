
class DataState {
    shouldAnimate: boolean = true;
    currentAngle: number = 0;
    animationSpeed: number = 90;
	maxRadius: number = 200;

    primaryColor = {r: 100, g: 100, b: 100}
    backgroundColor = {r: 0, g: 0, b: 0, a: 5}
	degreeMultiplier: number = 1;

}

export default DataState;
export {DataState};