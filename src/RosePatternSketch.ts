import p5 from "p5";
import { Pane } from "tweakpane";
import ConfigurationHelper from "./ConfigurationHelper";
import DataState from "./DataState";

class RosePatternSketch {
	
	private canvas?: p5.Renderer;
	private screenSize = 500;

	private data = new DataState()
	private configUi: Pane;

	private lastPosition?: {x: number, y: number}

	private clearCanvas: boolean = false;



	constructor(private p: p5){
		this.configUi = ConfigurationHelper.Setup(this.data);
		const btn = this.configUi.addButton({title: "Clear Canvas"})
		btn.on("click", () => {this.clearCanvas = true})
		p.setup = () => this.setup();
		p.draw = () => this.draw();
	}

	private setup() {
		this.canvas = this.p.createCanvas(this.screenSize, this.screenSize);
		this.canvas.parent("app");
	}

	private draw() {
		if(this.clearCanvas) {
			this.clearCanvas = false;
			this.p.background(this.data.backgroundColor.r * 255, this.data.backgroundColor.g * 255, this.data.backgroundColor.b * 255)
		}

		if(this.data.shouldAnimate) {
			this.p.background(this.data.backgroundColor.r * 255, this.data.backgroundColor.g * 255, this.data.backgroundColor.b * 255, this.data.backgroundColor.a * 255);
			const a = this.p.frameRate();
			if(a === 0 || Number.isNaN(a)) {
				return; // Skips first frame.
			}
			this.data.currentAngle += this.data.animationSpeed / a
		}

		const degToRad = this.data.currentAngle * (Math.PI/180)
		this.configUi.refresh()

		let r = Math.sin(this.data.degreeMultiplier*degToRad);

		let x = Math.sin(degToRad) * -this.data.maxRadius * r + this.screenSize / 2;
		let y = Math.cos(degToRad) * this.data.maxRadius * r + this.screenSize / 2;
		if(this.lastPosition) {
			this.p.stroke(this.data.primaryColor.r * 255, this.data.primaryColor.g * 255, this.data.primaryColor.b * 255)
			this.p.line(this.lastPosition.x, this.lastPosition.y, x, y)
		}
		this.lastPosition = {x,y}
	
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new RosePatternSketch(p5);
	}
}

export default RosePatternSketch;
